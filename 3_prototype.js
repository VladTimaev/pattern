const car = {
    wheels: 4,

    init () {
        console.log(`I have ${this.wheels} wheels, my owner ${this.owner}`) 
    }
}

const carWithOwner = Object.create(car, {
  owner: {
      value: 'Dima'
  }
})
console.log(carWithOwner.proto === car)
carWithOwner.init()